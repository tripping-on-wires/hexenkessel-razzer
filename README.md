# Hexenkessel Razzer

CD4096 pre-amp, tuned to work particularly well with DIY contact microphones (but would still boost most signals effectively).

Based upon circuits in Nicholas Collins' 'Handmade Electronic Music' with some modifications to suit contact mic's frequency responses and impedance needs.

This is still in prototype but sharing freely as I go.

It's named from 'Hexenkessel Razzer Kidder', one of the scribbles on The Fall's 'Hex Enduction Hour' cover. This is mostly a dumb reference to the use of a CMOS Hex Inverter (the CD4049).
